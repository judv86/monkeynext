// Import the functions you need from the SDKs you need

import { initializeApp } from "firebase/app";

// TODO: Add SDKs for Firebase products that you want to use

// https://firebase.google.com/docs/web/setup#available-libraries


// Your web app's Firebase configuration

const firebaseConfig = {

    apiKey: "AIzaSyB8evXhjnYJ4vN636MqowHzjl_3jnowyRI",

    authDomain: "projetnextmonkey.firebaseapp.com",

    projectId: "projetnextmonkey",

    storageBucket: "projetnextmonkey.appspot.com",

    messagingSenderId: "1022447178884",

    appId: "1:1022447178884:web:e9dcc10bea951c686eded1"
};


// Initialize Firebase

const app = initializeApp(firebaseConfig);
